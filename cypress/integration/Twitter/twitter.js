import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";
const MainPage = require('../pages/main.page.js')

const url = 'https://telnyx.com/'
const mainPage = new MainPage();
Given('I am on telnyx page', () => {
    cy.visit(url)
})
When(`I click Follow on Twitter`, () => {
    mainPage.clickAccept()
  })

Then('I should see a correct twitter url', () => {
    mainPage.twitterLinkShouldHaveText()    
  })