import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";
const MainPage = require('../pages/main.page.js')
const VoiceAPIPricingPage = require('../pages/voice.api.pricing.page')

const url = 'https://telnyx.com/'
const mainPage = new MainPage();
const voiceAPIPricingPage = new VoiceAPIPricingPage();
Given('I am on telnyx page', () => {
    cy.visit(url)
})
When(`I click on Voice API and choose Ukraine`, () => {
    mainPage.clickAccept()
    mainPage.clickVoiceAPIPricing()
    voiceAPIPricingPage.clickCountriesListButton()
    voiceAPIPricingPage.clickcountrieButton()
  })

Then('I should see a Voice API pricing for Ukraine', () => {
    cy.wait(3000)   
    voiceAPIPricingPage.headerShouldContain()
  })