import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";
const MainPage = require('../pages/main.page.js')
const LoginPage = require('../pages/login.page')


const url = 'https://telnyx.com/'
const mainPage = new MainPage();
const loginPage = new LoginPage();
Given('I am on telnyx page', () => {
    cy.visit(url)
})
When(`I click on log in and type email and password`, () => {
    mainPage.clickAccept()
    mainPage.clickLoginButton()
    loginPage.typeEmailInput('for.testing.15072022@gmail.com')
    loginPage.typePasswordInput('coIqZ36A!aaa')
    loginPage.clickLoginButton()
  })

Then('I should see a app page', () => {
    cy.wait(7000)
    cy.url().should('contain', '/app')
  })