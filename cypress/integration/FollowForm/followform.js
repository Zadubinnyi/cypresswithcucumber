import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";
const MainPage = require('../pages/main.page.js')
const BlogPage = require('../pages/blog.page')


const url = 'https://telnyx.com/'
const mainPage = new MainPage();
const blogPage = new BlogPage();
Given('I am on telnyx page', () => {
    cy.visit(url)
})
When(`I click on Blog and enter no valid email`, () => {
    mainPage.clickAccept()
    mainPage.clickBlogButton()
    blogPage.typeFollowFormInput('asfs')
    cy.get('[class="sc-348be244-0 jKLiMU"] p:first-child').click()
  })

Then('I should see a error message', () => {
    cy.wait(3000)
    blogPage.errorEmailShouldBeVisible()
  })