import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";
const MainPage = require('../pages/main.page.js')
const ContactUsPage = require('../pages/contactus.page')

const url = 'https://telnyx.com/'
const mainPage = new MainPage();
const contactUsPage = new ContactUsPage();
Given('I am on telnyx page', () => {
    cy.visit(url)
})
When(`I click “Talk to an expert” and submit`, () => {
    mainPage.clickAccept()
    mainPage.clickContactUsButton()
    cy.wait(3000)
    contactUsPage.clickSubmitButton()
  })

Then('I should see a erorr message', () => {
    cy.wait(3000)
    contactUsPage.formErrorShouldBeVisible() 
  })