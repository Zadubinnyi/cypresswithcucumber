import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";
const MainPage = require('../pages/main.page.js')
const SignupPage = require('../pages/signup.page')


const url = 'https://telnyx.com/'
const email = 'simple@gmail.com'
const mainPage = new MainPage();
const signupPage = new SignupPage();
Given('I am on telnyx page', () => {
  cy.visit(url)
})
When(`I type email and click "Try for free"`, () => {
    mainPage.clickAccept()
    mainPage.typeQuickEmailInput(email)
    mainPage.clickTryForFreeButton()
  })

Then('I should to see a sign up page where email input have value of the previously entered email', () => {
    cy.wait(3000)
    signupPage.EmailInputShouldHaveValue(email)
  })