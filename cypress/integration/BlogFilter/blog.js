import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";
const MainPage = require('../pages/main.page.js')
const BlogPage = require('../pages/blog.page')


const url = 'https://telnyx.com/'
const mainPage = new MainPage();
const blogPage = new BlogPage();
Given('I am on telnyx page', () => {
    cy.visit(url)
})
When(`I click on filters by product and content`, () => {
    mainPage.clickAccept()
    mainPage.clickBlogButton()
    blogPage.clickFilterByProductFAX()
    blogPage.clickFilterByContentTutorial()
  })

Then('I should see a filter result', () => {
    cy.wait(3000)
    blogPage.articleTypeShouldContain('Guides & Tutorials')
    blogPage.articleHeaderShouldContain('Fax')
  })