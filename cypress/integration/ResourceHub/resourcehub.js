import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";
const MainPage = require('../pages/main.page.js')
const ResourceHubPage = require('../pages/resourceHub.page')

const url = 'https://telnyx.com/'
const mainPage = new MainPage();
const resourceHubPage = new ResourceHubPage();
Given('I am on telnyx page', () => {
    cy.visit(url)
})
When(`I click on “Resource Hub” and one of the resource`, () => {
    mainPage.clickAccept()
    mainPage.clickResourceHubLink()
    resourceHubPage.clickSmsGuide()
  })

Then('I should see a resource guide', () => {
    cy.wait(3000)
    resourceHubPage.guideHeaderShouldContain()
  })