import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";
const MainPage = require('../pages/main.page.js')
const BlogPage = require('../pages/blog.page')


const url = 'https://telnyx.com/'
const searchData = 'API'
const mainPage = new MainPage();
const blogPage = new BlogPage();
Given('I am on telnyx page', () => {
    cy.visit(url)
})
When(`I click on blog and type search data`, () => {
    mainPage.clickAccept()
    mainPage.clickBlogButton()
    blogPage.typeSearchInput(searchData)
    blogPage.pressEnterForSearch()
  })

Then('I should see a search result on new page', () => {
    cy.wait(5000)
    cy.url().should('contain', searchData)
  })