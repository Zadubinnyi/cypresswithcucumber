import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";
const MainPage = require('../pages/main.page.js')
const IntegretionsPage = require('../pages/integration.page')

const url = 'https://telnyx.com/'
const mainPage = new MainPage();
const integretionsPage = new IntegretionsPage();
Given('I am on telnyx page', () => {
    cy.visit(url)
})
When(`I click on Integrations and submit`, () => {
    mainPage.clickAccept()
    mainPage.clickIntegrationsButton()
    integretionsPage.triggerForm()
  })

Then('I should see a error message', () => {
    cy.wait(3000)   
    integretionsPage.formToBeVisible()
  })