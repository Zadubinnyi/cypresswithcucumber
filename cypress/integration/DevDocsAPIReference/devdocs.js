import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";
const MainPage = require('../pages/main.page.js')
const DevDocsPage = require('../pages/devDocs.page.js')

const url = 'https://telnyx.com/'
const mainPage = new MainPage();
const devDocsPage = new DevDocsPage();
Given('I am on telnyx page', () => {
    cy.visit(url)
})
When(`I click on Developers Docs, API references and Node`, () => {
    mainPage.clickAccept()
    mainPage.clickDevDocsButton()
    devDocsPage.clickApiReferenceLink()
    devDocsPage.clickNodeButton()
  })

Then('I should see a API Reference for Node', () => {
    cy.wait(3000)
    devDocsPage.nodeButtonShouldHaveAttr()
  })