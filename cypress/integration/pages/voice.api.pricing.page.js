class VoiceAPIPricingPage {
    
    get countriesList(){
        return cy.get('[class="Text-sc-5o8owa-0 sc-7894861d-2 frufKM hIHHxS"] [class="sc-ecffda1a-4 dCDohY"]:first-child button')
        }

        clickCountriesListButton(){
            this.countriesList.click()
        }

    get countrie(){
        return cy.get('[href="/pricing/call-control/ua?currency=USD"]')
        }

        clickcountrieButton(){
            this.countrie.click()
        }

    get header(){
        return cy.get('[class="sc-9480303f-1 cirbUY"] h1')
    }

        headerShouldContain(){
            this.header.should('contain', 'Ukraine')
        }
        
    
}

export default VoiceAPIPricingPage
