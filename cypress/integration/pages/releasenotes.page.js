
class ReleaseNotesPage {
    
    get searchInput(){
        return cy.get('#search')
        }

        typeSearchInput(data){
            this.searchInput.type(data + '{enter}')
        }

    get voiceAPICheck(){
        return cy.get('label[for="voice api"]')
        }

        clickVoiceAPICheck(){
            this.voiceAPICheck.click()
        }

    get note(){
        return cy.get('[class="sc-d997c11f-0 kdZwDL"]:first-child')
        }

        noteShouldBeVisible(){
            this.note.should('be.visible')
        }

    get noteProduct(){
        return cy.get('[class="sc-d997c11f-0 kdZwDL"]:first-child [class="sc-a2ae9d5b-1 fOqIfy"]:first-child')
        }

        noteProductShouldContain(){
            this.noteProduct.should('contain', 'Voice API')
        }

      
    
}

export default ReleaseNotesPage
