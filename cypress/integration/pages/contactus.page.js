
class ContactUsPage {
    
    get submitButton(){
        return cy.get('[type="submit"]')
        }

        clickSubmitButton(){
            this.submitButton.click()
        }

    get formError(){
        return cy.get('[class="mktoError"]')
        }

        formErrorShouldBeVisible(){
            this.formError.should('be.visible')
        }

      
    
}

export default ContactUsPage
