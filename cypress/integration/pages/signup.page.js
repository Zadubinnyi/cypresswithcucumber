class SignupPage {
    
    get email(){
        return cy.get('#email')
        }

        typeEmailInput(email){
            this.email.type(email)
        }
        
        EmailInputShouldHaveValue(email){
            this.email.should('have.value', email)
        }

    get password(){
        return cy.get('#password')
        }

        typePasswordInput(pass){
            this.password.type(pass)
        }

    get submit(){
        return cy.get('button[type="submit"]')
        }

        clickSubmitButton(){
            this.submit.click()
        }

    get emailError(){
        return cy.get('#email_error')
    }
        emailErrorShouldBeVisible(){
            this.emailError.should('be.visible')
        }

    get passwordError(){
        return cy.get('#password_requirements')
    }
        passwordErrorShouldBeVisible(){
            this.passwordError.should('be.visible')
        }

    get fullNameError(){
        return cy.get('#full_name_error')
    }
        fullNameErrorShouldBeVisible(){
            this.fullNameError.should('be.visible')
        }
    
    
}

export default SignupPage
