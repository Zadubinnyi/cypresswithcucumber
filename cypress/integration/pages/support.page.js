
class SupportPage {
    
    get searchInput(){
        return cy.get('[class="search__input js__search-input o__ltr"]')
        }

        typeSearchInput(data){
            this.searchInput.type(data +'{enter}')
        }

    get searchResult(){
        return cy.get('[class="section__content"]')
        }

        searchResultShouldContain(data){
            this.searchResult.should('contain', data)
        }

      
    
}

export default SupportPage
