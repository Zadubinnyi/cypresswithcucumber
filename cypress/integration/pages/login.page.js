class LoginPage {
    
    get email(){
        return cy.get('[class="LoginForm__Content-jTQUFp bewpNc"] input[name="email"]')
        }

        typeEmailInput(email){
            this.email.type(email)
        }

    get password(){
        return cy.get('[name="password"]')
        }

        typePasswordInput(pass){
            this.password.type(pass)
        }

    get login(){
        return cy.get('form[aria-label="loginForm"] [type="submit"]')
        }

        clickLoginButton(){
            this.login.click()
        }

    get error(){
        return cy.get('[class="Message__MessageCopy-sc-1lbs5ge-2 ilxvtf"]')
    }
        errorShouldBeVisible(){
            this.error.should('be.visible')
        }
   
    
}

export default LoginPage
