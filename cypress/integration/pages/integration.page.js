class IntegretionsPage {
    
    get submitButton(){
        return cy.get('[class="mktoButtonRow"] button')
        }

        clickSubmitButton(){
            this.submitButton.click()
        }        
        
    get errorMessage(){
        return cy.get('#ValidMsgEmail')
        }

        errorMessageShouldBeVisible(){
            this.errorMessage.should('be.visible')
        } 
        
    get form(){
        return cy.get('#become-a-beta-tester')
        }

        triggerForm(){
            this.form.trigger('mouseover')
        } 

        formToBeVisible(){
            this.form.should('be.visible')
        } 
}

export default IntegretionsPage
