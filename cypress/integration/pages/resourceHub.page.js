class ResourceHubPage {
    
    get smsGuide(){
        return cy.get('[class="sc-3e56386e-3 epRMTm"] li:first-child [href="/learn/sms-guide"]')
        }

        clickSmsGuide(){
            this.smsGuide.click()
        }

    get guideHeader(){
        return cy.get('[class="sc-cfbede59-1 gmWGUW"] h1')
        }

        guideHeaderShouldContain(){
            this.guideHeader.should('contain', 'SMS')
        }

      
    
}

export default ResourceHubPage
