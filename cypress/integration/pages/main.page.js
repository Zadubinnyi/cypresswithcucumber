class MainPage {
    
    get accept(){
        return cy.get("[class='sc-5d3a275a-0 jdjrgE'] button")
        }

        clickAccept(){
            this.accept.click()
        }

    get signup(){
        return cy.get('li [class="sc-5d3a275a-0 eKznVb"] [href="/sign-up"]')
        }

        clickSignupButton(){
            this.signup.click()
        }

    get login(){
        return cy.get('[class="sc-602eb426-3 gnApio"] [href="https://portal.telnyx.com/"]:nth-child(4)')
        }

        clickLoginButton(){
            this.login.click()
        }

    get blog(){
        return cy.get('[class="sc-7b3980dc-2 bjOBlW"] [href="/resources"]')
        }

        clickBlogButton(){
            this.blog.click({ force: true })
        }

    get calculator(){
        return cy.get('[class="sc-7b3980dc-2 bjOBlW"] [href="/twilio-pricing-calculator"]')
        }

        clickCalculatorButton(){
            this.calculator.click({ force: true })
        }

    get voiceAPIPricing(){
        return cy.get('[class="sc-9d98fd33-4 fClzRu"] li:nth-child(10) [class="sc-7b3980dc-9 ctVOEX"] [href="/pricing/call-control"]')
        }

        clickVoiceAPIPricing(){
            this.voiceAPIPricing.click({ force: true })
        }
        
    get integrations(){
        return cy.get('[class="sc-9d98fd33-4 fClzRu"] li:nth-child(8) [class="sc-7b3980dc-9 ctVOEX"] [href="/integrations"]')
        }

        clickIntegrationsButton(){
            this.integrations.click({ force: true })
        }

    get quickEmailInput(){
        return cy.get('[type="email"]')
        }

        typeQuickEmailInput(data){
            this.quickEmailInput.type(data)
        }

    get tryForFreeButton(){
        return cy.get('[type="submit"]')
        }

        clickTryForFreeButton(){
            this.tryForFreeButton.click()
        }

    get devDocs(){
        return cy.get('[class="sc-7b3980dc-2 bjOBlW"] [href="https://developers.telnyx.com/docs"]')
        }

        clickDevDocsButton(){
            this.devDocs.click({ force: true })
        }

    get resourceHubLink(){
        return cy.get('[href="/learn"]')
        }

        clickResourceHubLink(){
            this.resourceHubLink.click()
        }

    get supportLink(){
        return cy.get('[href="https://support.telnyx.com"]:nth-child(3)')
        }

        clickSupportLink(){
            this.supportLink.click()
        }

    get twitterLink(){
        return cy.get('[class="sc-7b6c9f9b-2 jSHfbB"] div:nth-child(6) li:nth-child(2) a')
        }

        twitterLinkShouldHaveText(){
            this.twitterLink.should('have.attr', 'href', 'https://twitter.com/telnyx')
        }

    get contactUsButton(){
        return cy.get('[class="sc-9d98fd33-7 sc-9d98fd33-9 kpJVrZ ilKAnW"] [href="/contact-us"]')
        }

        clickContactUsButton(){
            this.contactUsButton.click()
        }

    get releaseNotesLink(){
        return cy.get('[href="/release-notes"]')
        }

        clickReleaseNotesLink(){
            this.releaseNotesLink.click()
        }
    
    
}

export default MainPage
