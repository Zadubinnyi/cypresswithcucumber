class BlogPage {
    
    get search(){
        return cy.get('#search')
        }

        typeSearchInput(data){
            this.search.type(data)
        }

        pressEnterForSearch(){
            this.search.type('{enter}')
        }

    get filterByProductFAX(){
        return cy.get('[aria-labelledby="filter-by-product"] li:nth-child(4)')
    }

        clickFilterByProductFAX(){
            this.filterByProductFAX.click()
        }

    get filterByContentTutorial(){
        return cy.get('[aria-labelledby="filter-by-content"] li:nth-child(2)')
    }

        clickFilterByContentTutorial(){
            this.filterByContentTutorial.click()
        }

    get articleType(){
        return cy.get('#articles a:first-child header')
    }

        articleTypeShouldContain(type){
            this.articleType.should('contain', type)
        }

    get articleHeader(){
        return cy.get('#articles a:first-child h2')
    }

        articleHeaderShouldContain(type){
            this.articleHeader.should('contain', type)
        }

    get followFormInput(){
        return cy.get('#Email')
    }

        typeFollowFormInput(data){
            this.followFormInput.type(data)
        }
        
    get errorEmail(){
        return cy.get('#Email_error')
    }

        errorEmailShouldBeVisible(){
            this.errorEmail.should('be.visible')
        }
  
    get followFormHeader(){
        return cy.get('[class="sc-348be244-0 jKLiMU"] p:first-child')
    }

        focuseFollowFormHeader(){
            this.followFormHeader.click()
        }
    
}

export default BlogPage
