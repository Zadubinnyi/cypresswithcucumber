class DevDocsPage {
    
    get v1Button(){
        return cy.get('[class="sc-9c6b9c3c-0 fsStYO"] [class="sc-58c8f912-0 hYETLD"] button:first-child')
        }

        clickv1Button(){
            this.v1Button.click()
        }

    get notice(){
        return cy.get('[class="sc-8591fb29-0 hpvsaJ"]')
        }

        noticeBeVisible(){
            this.notice.should('be.visible')
        }

    get searchInput(){
        return cy.get('[class="sc-9c6b9c3c-0 sFNkS"] input')
        }

        typeSearchInput(data){
            this.searchInput.type(data)
        }

    get searchList(){
        return cy.get('[class="sc-1029891a-4 geKsPq"]')
        }

        searchListBeVisible(){
            this.searchList.should('be.visible')
        }

    get apiReferenceLink(){
        return cy.get('[class="sc-4cec3818-5 NpAUG"] [href="/docs/api/v2/overview"]')
        }

        clickApiReferenceLink(){
            this.apiReferenceLink.click()
        }

    get nodeButton(){
        return cy.get('#tab-Node')
        }

        clickNodeButton(){
            this.nodeButton.click()
        }

        nodeButtonShouldHaveAttr(){
            this.nodeButton.should('have.attr', 'aria-selected', 'true')
        }

    get developmentLink(){
        return cy.get('[class="sc-9c6b9c3c-0 fsStYO"] a:last-child')
        }

        clickDevelopmentLink(){
            this.developmentLink.click()
        }

    get devEnvSetup(){
        return cy.get('#docs-desktop-sidebar [href="/docs/v2/development/dev-env-setup"]')
        }

        clickDevEnvSetup(){
            this.devEnvSetup.click()
        }

    get gitLink(){
        return cy.get('[class="sc-9d44e7fe-4 iVJLiA"] a')
        }

        gitLinkHaveAttr(){
            this.gitLink.should('have.attr', 'href', 'https://github.com/team-telnyx/telnyx-node')
        }


      
    
}

export default DevDocsPage
