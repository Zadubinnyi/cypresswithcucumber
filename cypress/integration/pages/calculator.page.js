class CalculatorPage {
    
    get messagingAPI(){
        return cy.get('[class="sc-a87e7459-1 gFVaeZ"]:first-child')
        }

        clickMessagingAPIButton(){
            this.messagingAPI.click()
        }

    get continue(){
        return cy.get('[class="sc-5d3a275a-0 eKznVb"] button')
    }

        clickContinueButton(){
            this.continue.click()
        }

    get localNumbers(){
        return cy.get('#local-numbers')
    }
        typeLocalNumbersInput(amount){
            this.localNumbers.type(amount)
        } 

    get tollFreeNumbers(){
        return cy.get('#toll-free-numbers')
    }

        typeTollFreeNumbersInput(amount){
                this.tollFreeNumbers.type(amount)
            }

    get saving(){
        return cy.get('[class="sc-c7d3cfaa-7 dSfmPD"] h2')
    }

        savingShouldContain(){
                this.saving.should('contain', 'Your savings')
            }
        
    
}

export default CalculatorPage
