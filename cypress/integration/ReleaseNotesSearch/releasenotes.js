import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";
const MainPage = require('../pages/main.page.js')
const ReleaseNotesPage = require('../pages/releasenotes.page')

const url = 'https://telnyx.com/'
const data ='IoT'
const mainPage = new MainPage();
const releaseNotesPage = new ReleaseNotesPage();
Given('I am on telnyx page', () => {
    cy.visit(url)
})
When(`I click “Realease Notes” and enter search data`, () => {
    mainPage.clickAccept()
    mainPage.clickReleaseNotesLink()
    releaseNotesPage.typeSearchInput(data)
  })

Then('I should see a note', () => {
    cy.wait(3000)
    releaseNotesPage.noteShouldBeVisible() 
  })