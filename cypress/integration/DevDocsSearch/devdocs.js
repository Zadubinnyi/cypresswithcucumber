import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";
const MainPage = require('../pages/main.page.js')
const DevDocsPage = require('../pages/devDocs.page.js')

const url = 'https://telnyx.com/'
const data = 'IoT'
const mainPage = new MainPage();
const devDocsPage = new DevDocsPage();
Given('I am on telnyx page', () => {
    cy.visit(url)
})
When(`I click on Developers Docs and enter search data`, () => {
    mainPage.clickAccept()
    mainPage.clickDevDocsButton()
    devDocsPage.typeSearchInput(data)
  })

Then('I should see a search result list', () => {
    cy.wait(3000)
    devDocsPage.searchListBeVisible()
  })