import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";
const MainPage = require('../pages/main.page.js')
const DevDocsPage = require('../pages/devDocs.page.js')

const url = 'https://developers.telnyx.com/docs/v2'
const devDocsPage = new DevDocsPage();
Given('I am on dev docs telnyx page', () => {
    cy.visit(url)
})
When(`I click Development Environment Setup and Github label`, () => {
    devDocsPage.clickDevelopmentLink()
    devDocsPage.clickDevEnvSetup()
  })

Then('I should see a github repository with SDK', () => {
    devDocsPage.gitLinkHaveAttr()
  })