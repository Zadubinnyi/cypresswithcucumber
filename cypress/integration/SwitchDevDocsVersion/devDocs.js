import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";
const MainPage = require('../pages/main.page.js')
const DevDocsPage = require('../pages/devDocs.page.js')

const url = 'https://telnyx.com/'
const mainPage = new MainPage();
const devDocsPage = new DevDocsPage();
Given('I am on telnyx page', () => {
    cy.visit(url)
})
When(`I click on Developers Docs and API v1`, () => {
    mainPage.clickAccept()
    mainPage.clickDevDocsButton()
    devDocsPage.clickv1Button()
  })

Then('I should see a notice', () => {
    cy.wait(3000)
    devDocsPage.noticeBeVisible()
  })