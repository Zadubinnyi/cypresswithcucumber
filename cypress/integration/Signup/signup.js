import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";
const MainPage = require('../pages/main.page.js')
const SignupPage = require('../pages/signup.page')


const url = 'https://telnyx.com/'
const mainPage = new MainPage();
const signupPage = new SignupPage();
Given('I am on telnyx page', () => {
  cy.visit(url)
})
When(`I click on signup`, () => {
    mainPage.clickAccept()
    mainPage.clickSignupButton()
  })

Then('I type not valid email and password', () => {
    signupPage.typeEmailInput('asd')
    signupPage.typePasswordInput('1a2b')
    signupPage.clickSubmitButton('1a2b')
    signupPage.emailErrorShouldBeVisible()
    signupPage.fullNameErrorShouldBeVisible()
    signupPage.passwordErrorShouldBeVisible()
  })