import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";
const MainPage = require('../pages/main.page.js')
const SupportPage = require('../pages/support.page')

const url = 'https://support.telnyx.com/en/'
const data = 'API'
const supportPage = new SupportPage();
Given('I am on telnyx page', () => {
    cy.visit(url)
})
When(`I enter search data`, () => {
    supportPage.typeSearchInput(data)
  })

Then('I should see a search result', () => {
    cy.wait(3000)
    supportPage.searchResultShouldContain(data)
  })