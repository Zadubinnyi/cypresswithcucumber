import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";
const MainPage = require('../pages/main.page.js')
const LoginPage = require('../pages/login.page')


const url = 'https://telnyx.com/'
const mainPage = new MainPage();
const loginPage = new LoginPage();
Given('I am on telnyx page', () => {
    cy.visit(url)
})
When(`I click on log in and type no valid email and password`, () => {
    mainPage.clickAccept()
    mainPage.clickLoginButton()
    loginPage.typeEmailInput('for.testing.15072022@gmail.com')
    loginPage.typePasswordInput('123')
    loginPage.clickLoginButton()
  })

Then('I should see a message saying that email and password combination is not valid', () => {
    cy.wait(2000)
    loginPage.errorShouldBeVisible()
  })