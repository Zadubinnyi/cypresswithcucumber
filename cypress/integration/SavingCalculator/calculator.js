import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";
const MainPage = require('../pages/main.page.js')
const CalculatorPage = require('../pages/calculator.page')



const url = 'https://telnyx.com/'
const amount = '50'
const mainPage = new MainPage();
const calculatorPage = new CalculatorPage();
Given('I am on telnyx page', () => {
    cy.visit(url)
})
When(`I click on saving calculator and choose product`, () => {
    mainPage.clickAccept()
    mainPage.clickCalculatorButton()
    calculatorPage.clickMessagingAPIButton()
    calculatorPage.clickContinueButton()
    calculatorPage.typeLocalNumbersInput(amount)
    calculatorPage.typeTollFreeNumbersInput(amount)
    calculatorPage.clickContinueButton()
  })

Then('I should see a calculate amount of saving', () => {
    cy.wait(3000)   
    calculatorPage.savingShouldContain()
  })